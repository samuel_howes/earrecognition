//
//  EARAppDelegate.h
//  EarRecognition
//
//  Created by Sam Howes on 4/3/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EARAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
