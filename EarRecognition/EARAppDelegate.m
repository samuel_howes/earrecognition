//
//  EARAppDelegate.m
//  EarRecognition
//
//  Created by Sam Howes on 4/3/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import "EARAppDelegate.h"
#import "EARPatientsDataStore.h"
#import "EARImageMatcher.h"

const BOOL initializeFromImages = NO;

@interface EARAppDelegate ()
{
    NSString *documentsDirectoryPath;
}
@end

@implementation EARAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    NSURL *dataPath = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
	documentsDirectoryPath = [dataPath path];
    
    // ==== ORIGINAL CODE =====
    //NSString *JSONPath = [documentsDirectoryPath stringByAppendingPathComponent:@"PatientsExported.json"];
    //NSString *XMLPath = [documentsDirectoryPath stringByAppendingPathComponent:@"OpenCVDatabase.xml"];

    // ==== New CODE to load from ImagesDatabase =====
    NSString *csvPath = [[NSBundle mainBundle] pathForResource:@"filenames" ofType:@"csv" inDirectory:kInputImageDatabaseDirectoryName];
    NSString *imagesDatabasePath = [csvPath stringByDeletingLastPathComponent];
    
    
    NSString *JSONPath = [imagesDatabasePath stringByAppendingPathComponent:@"PatientsExported.json"];
    NSString *XMLPath = [imagesDatabasePath stringByAppendingPathComponent:@"OpenCVDatabase.xml"];
    
    // ==== End New Code
    
    NSArray *descriptorsArray;
    
    /****************************** Start the data ********************************/
    /****** Section for calculating new data and immediately exporting **************/
    
    // Compute from images to bootstrap
    if (initializeFromImages)
    {
        descriptorsArray = [[EARImageMatcher sharedInstance] initializeFromImages];
        
        [[EARPatientsDataStore sharedInstance] importPatientsFromJSONFile:nil andOverrideWithDescriptorsArray:descriptorsArray];
        
        // manually set the paths for now.
        [[EARImageMatcher sharedInstance] setXMLDatabasePath:XMLPath];
        [[EARPatientsDataStore sharedInstance] setJSONDatabasePath:JSONPath];
        [[EARPatientsDataStore sharedInstance] setDescriptorsDatabaseFilename:[XMLPath lastPathComponent]];
        
        [[EARImageMatcher sharedInstance] exportDatabaseToXML:XMLPath withDescriptorNames:nil];
        [[EARPatientsDataStore sharedInstance] exportPatientsToJSONFile:JSONPath];
    }
    else
    {

        /****** Section for loading from stored databases and immediately exporting **************/
        
        // Load pre-stored data
        descriptorsArray = [[EARPatientsDataStore sharedInstance] importPatientsFromJSONFile:JSONPath];
        NSArray *patientIDsArray = [[EARPatientsDataStore sharedInstance] getPatientIDsArray];
        
        // ==== Original Commented Code Un-Comment this after it works ====
        //XMLPath = [documentsDirectoryPath stringByAppendingPathComponent:[[EARPatientsDataStore sharedInstance] descriptorsDatabaseFilename]];
        // === END Original Code
        
        [[EARImageMatcher sharedInstance] importDatabaseFromXML:XMLPath patientIDsArray:patientIDsArray withDescriptorNames:descriptorsArray];
        
        
        // ==== New code to re-save to Documents Directory ====
        
        JSONPath = [documentsDirectoryPath stringByAppendingPathComponent:@"PatientsExported.json"];
        XMLPath = [documentsDirectoryPath stringByAppendingPathComponent:@"OpenCVDatabase.xml"];
        
        [[EARImageMatcher sharedInstance] exportDatabaseToXML:XMLPath withDescriptorNames:nil];
        [[EARPatientsDataStore sharedInstance] exportPatientsToJSONFile:JSONPath];
        
        // ==== END New Code
        
    }
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //[[EARImageMatcher sharedInstance] exportDatabaseToXML:nil withDescriptorNames:nil];
    //});
    
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //[[EARPatientsDataStore sharedInstance] exportPatientsToJSONFile:nil];
    //});
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
