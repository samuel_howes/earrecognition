//
//  EARCameraOverlayView.h
//  EarRecognition
//
//  Created by Sam Howes on 4/26/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EARCameraOverlayView : UIView

@end
