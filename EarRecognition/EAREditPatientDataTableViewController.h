//
//  EAREditPatientDataViewController.h
//  EarRecognition
//
//  Created by Sam Howes on 4/27/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EARPatient.h"

@class EAREditPatientDataTableViewController;

@protocol EditPatientDataDelegate <NSObject>

- (void)editPatientDataTableViewController:(EAREditPatientDataTableViewController *)editPatientController didFinishEditingPatient:(EARPatient *)editedPatient;

@end

@interface EAREditPatientDataTableViewController : UITableViewController

@property (strong, nonatomic) EARPatient *patient;

@end
