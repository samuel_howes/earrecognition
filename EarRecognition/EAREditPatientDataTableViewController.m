//
//  EAREditPatientDataViewController.m
//  EarRecognition
//
//  Created by Sam Howes on 4/27/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import "EAREditPatientDataTableViewController.h"

@interface EAREditPatientDataTableViewController ()
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *DOBTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@end

@implementation EAREditPatientDataTableViewController
@synthesize patient;
@synthesize firstNameTextField;
@synthesize lastNameTextField;
@synthesize DOBTextField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    firstNameTextField.text = patient.firstName;
    lastNameTextField.text = patient.lastName;
    DOBTextField.text = patient.DOB;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (sender == self.cancelButton) return;
    else
    {
        patient.firstName = firstNameTextField.text;
        patient.lastName = lastNameTextField.text;
        patient.DOB = DOBTextField.text;
        [patient calculateAge];
    }
}



@end
