//
//  EARImageMatcher.h
//  EarRecognition
//
//  Created by Sam Howes on 4/25/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kInputImageDatabaseDirectoryName;

@class EARImageMatcher;

@protocol ImageMatcherDelegate <NSObject>

- (void)imageMatcher:(EARImageMatcher *)imageMatcher
didMatchImageToPossibleIDs:(NSArray *)patientIDs;

@end

@interface EARImageMatcher : NSObject

@property (strong, nonatomic) NSArray *patientIDsArrayFromLastMatch;

@property (strong, nonatomic) NSArray *distancesArrayFromLastMatch;

@property (strong, nonatomic) NSString *XMLDatabasePath;

@property (strong, nonatomic) NSMutableArray *descriptorNamesArray;

@property (weak, nonatomic) id<ImageMatcherDelegate> delegate;

+ (id)sharedInstance;

+ (UIImage *)UIImageFromFile:(NSString *)path;

- (UIImage *)getImageFromLastMatch;

- (NSArray *)initializeFromImages;

- (NSArray *)matchImageFromCamera:(UIImage *)imageToMatch;

- (UIImage *)processImage:(UIImage *)imageToAnalyze lowValue:(float)lowValue highValue:(float)highValue;

- (void)exportDatabaseToXML:(NSString *)XMLpath withDescriptorNames:(NSArray *)descriptorNamesArray;

- (void)importDatabaseFromXML:(NSString *)XMLPath patientIDsArray:(NSArray *)patientIDsArray withDescriptorNames:(NSArray *)descriptorNamesArray;

- (NSArray *)addLastMatchToDatabaseForPatientID:(NSNumber *)patientID;

@end
