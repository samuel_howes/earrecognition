//
//  EARImageMatcher.m
//  EarRecognition
//
//  Created by Sam Howes on 4/25/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import "EARImageMatcher.h"

#import <opencv2/opencv.hpp>
#import <opencv2/features2d/features2d.hpp>
#import <opencv2/nonfree/features2d.hpp>
#import <fstream>
using namespace cv;
using namespace std;

const int kEARMagicNumber = 10;
NSString *const kInputImageDatabaseDirectoryName = @"InputImageDatabase";

@interface EARImageMatcher ()
{
    SiftFeatureDetector detector;                   // performs actual SIFT computation on an image input
	BFMatcher matchmaker;                           // Brute Force Matcher: compares SIFT Features of test image and training database
    vector<vector<Mat>> descriptorsDatabase;        // Each index into this vector matches a corresponding entry int he patientIDs array
    vector<int> patientIDsArray;                    // Record the patient ID associated with each set of descriptors
    Mat descriptorsFromLastMatch;                   // Keep information on the last match around, in case we want to add it to the database
    Mat imageFromLastMatch;
}

@end

@implementation EARImageMatcher

@synthesize delegate;
@synthesize patientIDsArrayFromLastMatch;
@synthesize XMLDatabasePath;
@synthesize descriptorNamesArray;
@synthesize distancesArrayFromLastMatch;

+ (id) sharedInstance
{
    static EARImageMatcher *singleton = nil;
    if (singleton == nil)
    {
        singleton = [[EARImageMatcher alloc] init];
    }
    return singleton;
}

/****************************************************************************/
/*                              Life Cycle                                  */
/****************************************************************************/

- (id)init
{
    self = [super init];
    if (self)
    {
        int nFeatures = 0;
        int nOctaveLayers = 3;
        double contrastThreshold = 0.025;
        int edgeThreshold = 20;
        double sigma = 1.0;
        self->detector = SiftFeatureDetector(nFeatures, nOctaveLayers, contrastThreshold, edgeThreshold, sigma);
    }
    return self;
}

- (NSArray *)initializeFromImages
{
    NSLog(@"[EARImageMatcher] Initializing datastructures from the images database");
    
    vector<vector<string>> imageFileNames;
    vector<int> filenameIndexForImageIndex;
    
    NSString *csvPath = [[NSBundle mainBundle] pathForResource:@"filenames" ofType:@"csv" inDirectory:kInputImageDatabaseDirectoryName];
    NSString *imagesDatabasePath = [csvPath stringByDeletingLastPathComponent];
    
    NSLog(@"[EARImageMatcher] Reading in image filenames...");
    descriptorNamesArray = [self readCSV:&imageFileNames // Get the names of our input bitmaps
                                        path:[imagesDatabasePath stringByAppendingPathComponent:@"filenames.csv"]];
    
    
    vector<Mat> allImages;
    Mat imageFromDisk;
    NSLog(@"[EARImageMatcher] Loading input images for %lu Patients from disk...", imageFileNames.size());
    for (int i = 0; i < (int)imageFileNames.size(); i++)
    {
        self->descriptorsDatabase.push_back(vector<Mat>());         // Initialize the database with empty vectors so we can easily index later
        for (int jj = 0; jj < imageFileNames[i].size(); ++jj)
        {
            imageFromDisk = imread(imageFileNames[i][jj]);
            cvtColor(imageFromDisk, imageFromDisk, cv::COLOR_RGB2GRAY);
            //equalizeHist(imageFromDisk, imageFromDisk);
            allImages.push_back(imageFromDisk);
            filenameIndexForImageIndex.push_back(i);                // Record how to map back to the original vector
        }
	}
    

	NSLog(@"[EARImageMatcher] Detecting keypoints for %ld images...", allImages.size());
    
    vector<vector<KeyPoint>> allKeypoints;                          // Stores SIFT keypoints for each image in the database
    for(int i = 0; i < (int)allImages.size(); i++)
    {
		vector<KeyPoint> temp;
		self->detector.detect(allImages[i],temp);
		allKeypoints.push_back(temp);
	}
    
    NSLog(@"[EARImageMatcher] Computing descriptors for each of the %lu keypoint sets found...", allKeypoints.size());
    
	for(int i = 0;i < (int)allKeypoints.size(); i++)                // Compute descriptors for each keypoint and add them to the database
    {
		Mat temp;
		self->detector.compute(allImages[i], allKeypoints[i], temp);
        int descriptorsVectorIndex = filenameIndexForImageIndex[i];
        self->descriptorsDatabase[descriptorsVectorIndex].push_back(temp);
	}
    
    NSLog(@"[EARImageMatcher] Completed initialization with %ld Patients, and %lu images", patientIDsArray.size(), allImages.size());
    return descriptorNamesArray;
}

- (void)importDatabaseFromXML:(NSString *)XMLPath patientIDsArray:(NSArray *)inputPatientIDsArray withDescriptorNames:(NSArray *)patientDescriptorNamesArray
{
    XMLDatabasePath = XMLPath;
    descriptorNamesArray = [NSMutableArray arrayWithArray:patientDescriptorNamesArray];
    
    NSLog(@"[EARImageMatcher] Importing database of descriptors for %lu patients from XML File: %@", (unsigned long)[descriptorNamesArray count], XMLPath);
    
    descriptorsDatabase.clear();
    for (int ii = 0; ii < [patientDescriptorNamesArray count]; ii++)
    {
        int patientID = (int)[inputPatientIDsArray[ii] intValue];
        self->patientIDsArray.push_back(patientID);
        self->descriptorsDatabase.push_back(vector<Mat>());
    }
    
    FileStorage fs([XMLPath UTF8String],FileStorage::READ);
    ostringstream oss;
    Mat temp;
    int descriptorsCount = 0;
    for (int ii = 0; ii < [descriptorNamesArray count]; ++ii)
    {
        NSArray *patientDescriptors = [descriptorNamesArray objectAtIndex:ii];
        for (NSString *descriptorName in patientDescriptors)
        {
            descriptorsCount++;
            fs[[descriptorName UTF8String]] >> temp;
            descriptorsDatabase[ii].push_back(temp);
        }
    }
    
    fs.release();
    
    NSLog(@"[EARImageMatcher] Successfully imported %d descriptors", descriptorsCount);
}

- (void)exportDatabaseToXML:(NSString *)XMLPath withDescriptorNames:(NSArray *)patientDescriptorNamesArray
{
    if (!XMLPath)
    {
        XMLPath = XMLDatabasePath;
    }
    
    NSLog(@"[EARImageMatcher] Exporting database of %d descriptors to XML File: %@", (int)descriptorsDatabase.size(), XMLPath);
    
    
    Mat t;
    FileStorage f;
    f.open([XMLPath UTF8String], FileStorage::WRITE);
    
    
    stringstream outStream;
    ostringstream oss;
    for (int ii = 0; ii < descriptorsDatabase.size(); ++ii)
    {
        for (int jj = 0; jj < descriptorsDatabase[ii].size(); ++jj)
        {
        
            NSString *descriptorName = (NSString *)[(NSArray *)[descriptorNamesArray objectAtIndex:ii] objectAtIndex:jj];
        
            f << [descriptorName UTF8String] << descriptorsDatabase[ii][jj];
        }
    }
    f.release();
    
    NSLog(@"[EARImageMatcher] Database export successful.");
}

- (NSArray *)readCSV:(vector<vector<string>> *)fileNamesVector path:(NSString *)filePath
{
    if (!filePath)
    {
        @throw [NSException exceptionWithName:@"PathNotFoundException" reason:@"" userInfo:nil];
    }
    
    NSMutableArray *outputDescriptorsArray = [NSMutableArray new];
    vector<string> curPatientFilenames;
    
    ifstream infile([filePath UTF8String]);
	int index = 0;
    
    string path = string([[filePath stringByDeletingLastPathComponent] UTF8String]);
	if(infile)
    {
		string line;
        NSMutableArray *currentPatientsDescriptors;
		while(getline(infile,line))
        {
			istringstream sep(line);
			string filename;
            int prevPatientNumber = -1;
            int curPatientNumber = -1;
            string delimiter = "_";

			while(getline(sep, filename,','))
            {
                curPatientNumber =  atoi(filename.substr(0, filename.find(delimiter)).c_str());
                
                if (curPatientNumber != prevPatientNumber)
                {
                    patientIDsArray.push_back(curPatientNumber);
                    if (currentPatientsDescriptors != nil && [currentPatientsDescriptors count] != 0)
                    {
                        [outputDescriptorsArray addObject:currentPatientsDescriptors];
                        fileNamesVector->push_back(curPatientFilenames);
                    }
                    curPatientFilenames = vector<string>();
                    currentPatientsDescriptors = [NSMutableArray new];
                }
                
                curPatientFilenames.push_back(path + '/' + filename);
                [currentPatientsDescriptors addObject:[NSString stringWithFormat:@"descriptor_%@",
                                                       [[NSString stringWithUTF8String:filename.c_str()] stringByDeletingPathExtension]]];
                
                prevPatientNumber = curPatientNumber;
                index++;
			}
            
		}
        
        // Add the final array, as we will exit the loop before it is added
        if (currentPatientsDescriptors != nil && [currentPatientsDescriptors count] != 0)
        {
            [outputDescriptorsArray addObject:currentPatientsDescriptors];
            fileNamesVector->push_back(curPatientFilenames);
        }
	}
    return outputDescriptorsArray;
}

- (NSArray *)flatArrayFromPossiblyNestedArray:(NSArray *)inputArray
{
    // Allow for a flat array, as well as an array of nested items as from EARPatientsDataStore
    if (![[inputArray firstObject] isKindOfClass:[NSString class]])
    {
        NSMutableArray *flatArray = [NSMutableArray new];
        for (NSArray *nestedArray in inputArray)
        {
            for (NSString *descriptorName in nestedArray)
            {
                [flatArray addObject:descriptorName];
            }
        }
        return flatArray;
    }
    else
    {
        return inputArray;
    }
}

/****************************************************************************/
/*                              Image Analysis                              */
/****************************************************************************/

- (NSArray *)matchImageFromCamera:(UIImage *)imageToMatch
{
    NSLog(@"[EARImageMatcher] Matching camera image to an image in the database");
    
    NSLog(@"[EARImageMatcher] Computing keypoints and descriptors of image...");
    
    // Convert captured frame to grayscale
    Mat cvImageToMatch;
    cvtColor([self cvMatFromUIImage:imageToMatch], cvImageToMatch, cv::COLOR_RGB2GRAY);
    
    cv::Size newSize(50,180);															//create a new size (width,height)
    resize(cvImageToMatch, cvImageToMatch, newSize, 0, 0, INTER_LANCZOS4);				//resize the image
    
    imageFromLastMatch = cvImageToMatch;
    //equalizeHist(cvImageToMatch,cvImageToMatch);
    
    
    // DEBUGGING TESTING IMAGE
    //NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"042_1" ofType:@"bmp" inDirectory:kInputImageDatabaseDirectoryName];
   // cvImageToMatch = imread([imagePath UTF8String]);
    // END DEBUGGING
    
    vector<KeyPoint> keypointsForInputImage;
    Mat descriptorMatrixForInputImage;
    self->detector.detect(cvImageToMatch, keypointsForInputImage);
    self->detector.compute(cvImageToMatch, keypointsForInputImage, descriptorMatrixForInputImage);
    self->descriptorsFromLastMatch = descriptorMatrixForInputImage;
    
    vector<vector<DMatch>> allMatches;                                                  // Vector to store the matching data
    vector<bool> matchVector;
    vector<int> patientIDForMatchIndex;                                     // Vector to map an index from an index in the allMatches vector to a patient ID
    NSLog(@"[EARImageMatcher] Computing match values for %lu keypoints against every image...", keypointsForInputImage.size());
    for (int i = 0; i < self->descriptorsDatabase.size(); i++)              // Compute how well the camera image matches every image in the database
    {
        for (int jj = 0; jj < self->descriptorsDatabase[i].size(); ++jj)
        {
            vector<DMatch> temp;                // DMatch = a distance match type: stores important data
            
            bool shouldPushFalse = false;
            Mat databaseMatrix = self->descriptorsDatabase[i][jj];
            if (databaseMatrix.data != NULL)
            {
                self->matchmaker.match(descriptorMatrixForInputImage, databaseMatrix, temp);
                allMatches.push_back(temp);
            }
            else
            {
                shouldPushFalse = true;
            }
            
            if(!temp.empty() && shouldPushFalse != true)
            {
                matchVector.push_back(true);
                patientIDForMatchIndex.push_back(patientIDsArray[i]);
            }
            else
            {
                matchVector.push_back(true);
                cout << "[EARImageMatcher OpenCV] Match Matrix is empty for image: " << jj << " of Patient with index " << i << endl;
            }
        }
	}

    drawKeypoints(imageFromLastMatch, keypointsForInputImage, imageFromLastMatch);
    
    // Now allMatches is a Ragged matrix containing match information in each row.
    // length allMatches = number of images in the database ~ 500
    
    vector<float>vectEuclidDistSums;
	vector<float>vectSortedEuclidDistSums;      // This is used to store the sorted indexes of vectEuclidDistSums
    // Sum the values in each row to condense the ragged matrix into a column vector
    for (int ii = 0; ii < allMatches.size(); ++ii)
    {
		float tempSum = 0;
        if (matchVector[ii] == true)
        {
            for (int jj = 0; jj < allMatches[ii].size(); jj++)
            {
                tempSum += allMatches[ii][jj].distance;
            }
        }
        else
        {
            tempSum = FLT_MAX;
        }
        
		vectEuclidDistSums.push_back(tempSum); // result vector
	}
    
    Mat matEuclidDistSums = Mat(vectEuclidDistSums);        // Convert to a cv::Mat for convenience
    Mat matSortedEuclidDistSums;
    sortIdx(matEuclidDistSums, matSortedEuclidDistSums, CV_SORT_EVERY_COLUMN + CV_SORT_ASCENDING); // Sort
    
    
    /********************************* Report and record the results *************************/
    int descriptorIndex = (int)matSortedEuclidDistSums.at<int>(0);
    int patientID = patientIDForMatchIndex[descriptorIndex];
    
    NSLog(@"[EARImageMatcher] The best match was for descriptor at index: %d corresponding to Patient with ID: %d", descriptorIndex, patientID);
    NSMutableArray *tempDistances = [NSMutableArray new];
    NSMutableArray *tempPatientIDs = [NSMutableArray new];                  // Grab a temporary mutable array so we can add to it.
    for (int ii = 0; ii < kEARMagicNumber; ++ii)
    {
        descriptorIndex = (int)matSortedEuclidDistSums.at<int>(ii);
        float euclidDistance = (float)matEuclidDistSums.at<float>(descriptorIndex);
        patientID = patientIDForMatchIndex[descriptorIndex];
        [tempDistances addObject:[NSNumber numberWithDouble:euclidDistance]];
        [tempPatientIDs addObject:[NSNumber numberWithInt:patientID]];
    }
    distancesArrayFromLastMatch = tempDistances;
    patientIDsArrayFromLastMatch = tempPatientIDs;                          // commit the mutable array's changes
    
    // Our delegate will be a view controller, so we must execute on the main thread;
    dispatch_async(dispatch_get_main_queue(), ^{
        [delegate imageMatcher:self didMatchImageToPossibleIDs:patientIDsArrayFromLastMatch];
    });
    
    return patientIDsArrayFromLastMatch;
}


/* Used only as a proof of concept demo */
- (UIImage *)processImage:(UIImage *)imageToAnalyze lowValue:(float)lowValue highValue:(float)highValue;
{
    [self matchImageFromCamera:imageToAnalyze];
    Mat grayFrame, output;
    Mat tempMat = [self cvMatFromUIImage:imageToAnalyze];
    
    // Convert captured frame to grayscale
    cvtColor(tempMat, grayFrame, cv::COLOR_RGB2GRAY);
    
    // Perform Canny edge detection using slide values for thresholds
    /*Canny(grayFrame, output,
              lowValue * kCannyAperture * kCannyAperture,
              highValue * kCannyAperture * kCannyAperture,
              kCannyAperture);*/
    
    // return the result
    return [self UIImageFromCVMat:output];
    
}


- (NSArray *)addLastMatchToDatabaseForPatientID:(NSNumber *)newPatientID
{
    NSMutableArray *newDescriptorNamesForPatient = [NSMutableArray new];
    vector<Mat> newDescriptors;
    int patientID = [newPatientID intValue];
    int localIndexForPatient = -1;
    for (int ii = 0; ii < patientIDsArray.size(); ++ii)
    {
        if (patientIDsArray[ii] == patientID)
        {
            localIndexForPatient = ii;
        }
    }
    
    // If we are adding a new patient
    if (localIndexForPatient == -1)
    {
        // Append to the patient's array
        newDescriptors.push_back(descriptorsFromLastMatch);
        patientIDsArray.push_back(patientID);
        descriptorsDatabase.push_back(newDescriptors);
        [newDescriptorNamesForPatient addObject:[NSString stringWithFormat:@"descriptor_%d_%d", patientID, 1]];
        [descriptorNamesArray addObject:newDescriptorNamesForPatient];
    }
    else
    {
        // Add to existing patient
    }
    
    // Return the new descriptors
    return newDescriptorNamesForPatient;
}

/****************************************************************************/
/*                         OpenCV to iOS Interface                          */
/****************************************************************************/

- (Mat)cvMatFromUIImage:(UIImage *)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}

- (Mat)cvMatGrayFromUIImage:(UIImage *)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC1); // 8 bits per component, 1 channels
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}

-(UIImage *)UIImageFromCVMat:(cv::Mat)cvMat
{
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    CGColorSpaceRef colorSpace;
    
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                 //width
                                        cvMat.rows,                                 //height
                                        8,                                          //bits per component
                                        8 * cvMat.elemSize(),                       //bits per pixel
                                        cvMat.step[0],                              //bytesPerRow
                                        colorSpace,                                 //colorspace
                                        kCGImageAlphaNone|kCGBitmapByteOrderDefault,// bitmap info
                                        provider,                                   //CGDataProviderRef
                                        NULL,                                       //decode
                                        false,                                      //should interpolate
                                        kCGRenderingIntentDefault                   //intent
                                        );
    
    
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    
    return finalImage;
}

- (UIImage *)getImageFromLastMatch
{
    return [self UIImageFromCVMat:imageFromLastMatch];
}

+ (UIImage *)UIImageFromFile:(NSString *)path
{
    Mat imageFromDisk = imread([path UTF8String]);
    Mat temp;
    //cvtColor(imageFromDisk, imageFromDisk, cv::COLOR_RGB2GRAY);
    //equalizeHist(imageFromDisk,imageFromDisk);
    
    return [[EARImageMatcher sharedInstance] UIImageFromCVMat:imageFromDisk];
}

@end
