//
//  EARImageMatcherViewController.h
//  EarRecognition
//
//  Created by Sam Howes on 4/3/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EARPatient.h"

@class EARImageMatcherViewController;

@protocol ImageMatcherControllerDelegate <NSObject>

- (void)imageMatcherViewControler:(EARImageMatcherViewController *)imageMatcherViewController
           didMatchImageToPatient:(EARPatient *)patient;

@end


@interface EARImageMatcherViewController : UIViewController

@property (weak, nonatomic) id<ImageMatcherControllerDelegate> delegate;

- (UIImagePickerController *)getImagePickerController;

@end


