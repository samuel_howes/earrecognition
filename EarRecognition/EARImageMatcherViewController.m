//
//  EARImageMatcherViewController.m
//  EarRecognition
//
//  Created by Sam Howes on 4/3/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>
#import "EARImageMatcherViewController.h"
#import "EARImageMatcher.h"
#import "EARPatientsDataStore.h"
#import "EAREditPatientDataTableViewController.h"

NSString *const kTestImageToLoad = @"127_02";

NSString *const kDisplayMatchResultsSegue = @"DisplayMatchResultsSegue";
NSString *const kConfigureNewPatientSegue = @"ConfigureNewPatientSegue";

@interface EARImageMatcherViewController () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ImageMatcherDelegate>
{
    BOOL shouldShowImagePicker;
    BOOL didPerformMatch;
    NSString *matchButtonText;
    UIColor *matchButtonBackgroundColor;
}

- (IBAction)cancelButtonWasPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) UIImage *imageToAnalyze;

@property (nonatomic) IBOutlet UIView *cameraOverlayView;

@property UIImagePickerController *cameraUI;

@property (nonatomic) CGRect earBoundingBox;

@property (weak, nonatomic) IBOutlet UIButton *matchToDatabaseButton;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)matchButtonWasPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *cameraLoadingView;

@property (weak, nonatomic) IBOutlet UIButton *addNewPatientButton;

@end

@implementation EARImageMatcherViewController
@synthesize cameraUI;
@synthesize imageView;
@synthesize imageToAnalyze;
@synthesize earBoundingBox;
@synthesize delegate;
@synthesize activityIndicator;
@synthesize matchToDatabaseButton;
@synthesize cameraLoadingView;
@synthesize addNewPatientButton;

- (void)viewDidLoad
{
   // NSURL *dataPath = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
	//dataPath = [dataPath URLByAppendingPathComponent:@"SavedData.xml"];
    
    //NSArray *descriptorNames = [[EARImageMatcher sharedInstance] initializeFromImages];
    /*[[EARImageMatcher sharedInstance] exportDatabaseToXML:[dataPath path] withDescriptorNames:descriptorNames];
    [[EARImageMatcher sharedInstance] importDatabaseFromXML:[dataPath path] withDescriptorNames:descriptorNames]; */
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:kTestImageToLoad ofType:@"bmp" inDirectory:kInputImageDatabaseDirectoryName];
    
    imageToAnalyze = [EARImageMatcher UIImageFromFile:imagePath];
//    imageToAnalyze = [UIImage imageWithContentsOfFile:imagePath];
    shouldShowImagePicker = YES;
    [addNewPatientButton setHidden:YES];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [imageView setImage:imageToAnalyze];
    if (shouldShowImagePicker)
    {
        [cameraLoadingView setBackgroundColor:[UIColor blackColor]];
        [cameraLoadingView setHidden:NO];
        [self.view bringSubviewToFront:cameraLoadingView];
        [self.navigationController setNavigationBarHidden:YES];
    }
    else
    {
        cameraLoadingView.hidden = YES;
        [self.view sendSubviewToBack:cameraLoadingView];
        [self.navigationController setNavigationBarHidden:NO];
    }

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (shouldShowImagePicker)
        {
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
        }
    });
}

- (void)updateImageDisplay
{
    NSString *csvPath = [[NSBundle mainBundle] pathForResource:@"filenames" ofType:@"csv" inDirectory:kInputImageDatabaseDirectoryName];
    NSString *imagesDatabasePath = [csvPath stringByDeletingLastPathComponent];
    
    // Process the image
    
}

- (IBAction)cameraButtonWasPressed:(id)sender
{
    if (imageToAnalyze == nil)
    {
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    else
    {
        UIActionSheet *chooseNewImageActionSheet =
            [[UIActionSheet alloc] initWithTitle:nil delegate:self
                               cancelButtonTitle:@"Cancel"
                          destructiveButtonTitle:@"Take new photo"
                               otherButtonTitles:nil];
         
         [chooseNewImageActionSheet showFromBarButtonItem:sender animated:YES];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	switch (buttonIndex) {
		case 0: 			// Take Photo
			[self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
			break;
		case 1:				// Cancel
			break;
		default:
			break;
	}
}
/*
- (UIImagePickerController *)getImagePickerController
{
	cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [cameraUI setDelegate:self];
    [cameraUI setSourceType:UIImagePickerControllerSourceTypeCamera];
    
    cameraUI.showsCameraControls = YES;
	cameraUI.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage, nil];
	cameraUI.allowsEditing = YES;
    
    // Configure the OverlayView for the camera
    [[NSBundle mainBundle] loadNibNamed:@"CameraOverlayView" owner:self options:nil];
    UIView *earBox = [self.cameraOverlayView viewWithTag:1];
    self.earBoundingBox = [earBox viewWithTag:1].frame;
    
    // The earBox is currently a solid rectangle
    // Reconfigure it to be a bounding box with the border on the
    //  outside of the box so we know the exact size of the image.
    CGFloat borderWidth = 5.0f;
    earBox.frame = CGRectInset(earBox.frame, -borderWidth, -borderWidth);
    earBox.layer.borderWidth = borderWidth;
    
    earBox.layer.borderColor = earBox.layer.backgroundColor;
    [earBox setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    
    [cameraUI setCameraOverlayView:self.cameraOverlayView];
    self.cameraOverlayView = nil;
    
	[[UIApplication sharedApplication] setStatusBarHidden:YES];
	return cameraUI;
}
*/
- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
	cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.modalTransitionStyle = (shouldShowImagePicker ? UIModalTransitionStyleCrossDissolve: UIModalTransitionStyleCoverVertical);
    [cameraUI setDelegate:self];
    [cameraUI setSourceType:sourceType];
    
    cameraUI.showsCameraControls = YES;
	cameraUI.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage, nil];
	cameraUI.allowsEditing = YES;
    
    // Configure the OverlayView for the camera
    [[NSBundle mainBundle] loadNibNamed:@"CameraOverlayView" owner:self options:nil];
    UIView *earBox = [self.cameraOverlayView viewWithTag:1];
    self.earBoundingBox = [earBox viewWithTag:1].frame;
    
    // The earBox is currently a solid rectangle
    // Reconfigure it to be a bounding box with the border on the
    //  outside of the box so we know the exact size of the image.
    CGFloat borderWidth = 5.0f;
    earBox.frame = CGRectInset(earBox.frame, -borderWidth, -borderWidth);
    earBox.layer.borderWidth = borderWidth;
    
    earBox.layer.borderColor = earBox.layer.backgroundColor;
    [earBox setBackgroundColor:[UIColor colorWithWhite:1 alpha:0]];
    
    [cameraUI setCameraOverlayView:self.cameraOverlayView];
    self.cameraOverlayView = nil;

	[[UIApplication sharedApplication] setStatusBarHidden:YES];
	[self presentViewController:cameraUI animated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    shouldShowImagePicker = NO;
	[self dismissViewControllerAnimated:YES completion:nil];
	[[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
	UIImage *originalImage, *editedImage, *imageToSave;
	
	if (CFStringCompare((CFStringRef)mediaType, kUTTypeImage, 0) == kCFCompareEqualTo)
	{
		editedImage = 	(UIImage *)[info objectForKey:UIImagePickerControllerEditedImage];
		originalImage = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
		imageToSave = originalImage;
		
        // Save the new image (original or edited) to the Camera Roll
        //UIImageWriteToSavedPhotosAlbum (imageToSave, nil, nil , nil);
		//imageToAnalyze = imageToSave;
        imageToAnalyze = editedImage;
	}
	
	if (CFStringCompare ((CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo)
	{
		NSLog(@"Error: movie encounterd as a result of the image picker!");
	}
    
    // First crop the image to the size that we need
    // We're hoping that the user chose the image correctly
    
    // calculate the height of the image for screen coordiantes
    CGFloat imageAspectRatio = imageToAnalyze.size.height/imageToAnalyze.size.width;
    CGFloat imageHeightInScreenCoordinates = imageAspectRatio * cameraUI.view.bounds.size.width;

    // Now we are done with the cameraUI
    shouldShowImagePicker = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    cameraUI = nil;
    
    // Convert the earBoundingBox from screen coordinates to image coordinates
    CGFloat scaleFactor = imageToAnalyze.size.height / imageHeightInScreenCoordinates;
    
    earBoundingBox.size.height *= scaleFactor;
    earBoundingBox.size.width *= scaleFactor;
    
    // Swap the width and the height because the CGImage is weird
    //CGFloat temp = earBoundingBox.size.height;
    //earBoundingBox.size.height = earBoundingBox.size.width;
    //earBoundingBox.size.width = temp;
    
    // ... yeah. The height and the width is mixed into the x and y coordinates in different ways
    // This is how it works. I don't know why though, so don't ever change this.
    earBoundingBox.origin.x = imageToAnalyze.size.height/2 - earBoundingBox.size.width/2;
    earBoundingBox.origin.y = imageToAnalyze.size.width/2 - earBoundingBox.size.height/2;

    // Perform the actual cropping
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToAnalyze CGImage], self.earBoundingBox);
    imageToAnalyze = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    // Finally: rotate the image so we can display it and pass it to the ImageMatcher properly
    //UIImage* rotatedImage = [UIImage imageWithCGImage:imageToAnalyze.CGImage scale:1.0 orientation:UIImageOrientationRight];
    //imageToAnalyze = rotatedImage;
    
    /********************************* INSERT NEW LOGIC HERE ****************************************/
    //UIImageWriteToSavedPhotosAlbum (imageToAnalyze, nil, nil , nil);
    UIImageWriteToSavedPhotosAlbum (originalImage, nil, nil , nil);
    [imageView setImage:imageToAnalyze];
    [self setButtonEnabled:YES];
    [addNewPatientButton setHidden:YES];
    didPerformMatch = NO;
}

- (void)setButtonEnabled:(BOOL)enabled
{
    if (enabled)
    {
        if (matchButtonBackgroundColor)
        {
            matchToDatabaseButton.backgroundColor = matchButtonBackgroundColor;
        }
        if (matchButtonText)
        {
            [matchToDatabaseButton setTitle:matchButtonText forState:UIControlStateNormal];
        }
        matchToDatabaseButton.enabled = YES;
    }
    else
    {
        matchButtonBackgroundColor = matchToDatabaseButton.backgroundColor;
        matchButtonText = matchToDatabaseButton.titleLabel.text;
        
        matchToDatabaseButton.backgroundColor = [UIColor grayColor];
        [matchToDatabaseButton setTitle:@"" forState:UIControlStateDisabled];
        matchToDatabaseButton.enabled = NO;
    }
}

- (IBAction)cancelButtonWasPressed:(id)sender
{
    [delegate imageMatcherViewControler:self didMatchImageToPatient:nil];
}

- (IBAction)matchButtonWasPressed:(id)sender
{
    if (didPerformMatch)
    {
        [self performSegueWithIdentifier:kDisplayMatchResultsSegue sender:self];
    }
    else
    {
        [self setButtonEnabled:NO];
        [activityIndicator startAnimating];
        [[EARImageMatcher sharedInstance] setDelegate:self];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[EARImageMatcher sharedInstance] matchImageFromCamera:imageToAnalyze];
        });
    }
}

- (IBAction)addNewPatientButtonWasPressed:(id)sender
{
    [self performSegueWithIdentifier:kConfigureNewPatientSegue sender:self];
}

- (void)imageMatcher:(EARImageMatcher *)imageMatcher didMatchImageToPossibleIDs:(NSArray *)patientIDs
{
    [activityIndicator stopAnimating];
    didPerformMatch = YES;
    
    
    if (!patientIDs)
    {
        matchToDatabaseButton.backgroundColor = [UIColor redColor];
        [matchToDatabaseButton setTitle:@"Patient not found" forState:UIControlStateDisabled];
    }
    else
    {
        [matchToDatabaseButton setTitle:@"Match Successful" forState:UIControlStateNormal];
        [matchToDatabaseButton setBackgroundColor:matchButtonBackgroundColor];
        [matchToDatabaseButton setEnabled:YES];
        [self performSegueWithIdentifier:kDisplayMatchResultsSegue sender:self];
    }
    [addNewPatientButton setHidden:NO];
    imageToAnalyze = [[EARImageMatcher sharedInstance] getImageFromLastMatch];
    [imageView setImage:imageToAnalyze];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kConfigureNewPatientSegue])
    {
        EARPatient *newPatient = [[EARPatientsDataStore sharedInstance] createNewPatient];

        NSArray *descriptorNamesArray = [[EARImageMatcher sharedInstance] addLastMatchToDatabaseForPatientID:[newPatient patientID]];
        
        [newPatient setSIFTEarDescriptorNamesArray:descriptorNamesArray];
        
        [[EARPatientsDataStore sharedInstance] commitNewPatient:newPatient];
        UINavigationController *navController = [segue destinationViewController];
        [(EAREditPatientDataTableViewController *)[navController topViewController] setPatient:newPatient];
    }
}

- (IBAction)unwindToImageDisplay:(UIStoryboardSegue*)sender
{
    // Do NOTHING!
}

@end
