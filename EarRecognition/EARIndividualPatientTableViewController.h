//
//  EARIndividualPatientTableViewController.h
//  EarRecognition
//
//  Created by Sam Howes on 4/27/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EARPatient.h"
#import "EARMedicalHistoryItem.h"

@interface EARIndividualPatientTableViewController : UITableViewController

@property (strong, nonatomic) EARPatient *patient;

@end
