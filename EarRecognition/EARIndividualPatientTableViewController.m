//
//  EARIndividualPatientTableViewController.m
//  EarRecognition
//
//  Created by Sam Howes on 4/27/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import "EARIndividualPatientTableViewController.h"
#import "EAREditPatientDataTableViewController.h"

const int kAgeViewTag       = 1;
const int kHeightViewTag    = 2;
const int kWeightViewTag    = 3;
const int kSexViewTag       = 4;
const int kItemTypeViewTag  = 5;
const int kItemNameViewTag  = 6;
const int kItmeDateViewTag  = 7;

NSString *const kEditPatientDataSegueIdentifier = @"EditPatientData";

@interface EARIndividualPatientTableViewController ()

@property (weak, nonatomic) IBOutlet UILabel *ageLabel;

@property (weak, nonatomic) IBOutlet UILabel *heightLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UILabel *sexLabel;

@end

@implementation EARIndividualPatientTableViewController
@synthesize patient;

- (void) viewWillAppear:(BOOL)animated
{
    [self.navigationItem setTitle:[NSString stringWithFormat:@"%@ %@", patient.firstName, patient.lastName]];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 1)
    {
        return 1;
    }
    else
    {
        NSInteger numberOfHistoryItems = [[patient medicalHistory] count];
        if (numberOfHistoryItems == 0)
        {
            return 1;
        }
        else
        {
            return numberOfHistoryItems;
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *const kBasicInformationCellIdentifier = @"BasicInformationCell";
    static NSString *const kMedicalHistoryCellIdentifier = @"MedicalHistoryCell";
    static NSString *const kNoMedicalHistoryDataCellIdentifier = @"NoMedicalHistoryDataCell";
    UITableViewCell *cell;
    
    if (indexPath.section == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:kBasicInformationCellIdentifier forIndexPath:indexPath];
        [(UILabel *)[cell viewWithTag:kAgeViewTag] setText:[NSString stringWithFormat:@"%@", [patient age]]];
    }
    else
    {
        if ([[patient medicalHistory] count] == 0)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:kNoMedicalHistoryDataCellIdentifier forIndexPath:indexPath];
        }
        else
        {
            EARMedicalHistoryItem *historyItem = [[patient medicalHistory] objectAtIndex:indexPath.row];
            NSString *itemType = [EARMedicalHistoryItem itemTypeStringForEnumValue:historyItem.itemType];
            
            cell = [tableView dequeueReusableCellWithIdentifier:kMedicalHistoryCellIdentifier forIndexPath:indexPath];
            [(UILabel *)[cell viewWithTag:kItemTypeViewTag] setText:itemType];
            [(UILabel *)[cell viewWithTag:kItemNameViewTag] setText:historyItem.itemName];
            [(UILabel *)[cell viewWithTag:kItmeDateViewTag] setText:historyItem.itemDate];
        }
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return @"Basic Info";
    }
    else if (section == 1)
    {
        return @"Medical History";
    }
    else
    {
        return @"INVALID PROGRAMMER";
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kEditPatientDataSegueIdentifier])
    {
        UINavigationController *destController = [segue destinationViewController];
        [(EAREditPatientDataTableViewController *)[destController topViewController] setPatient:patient];
    }
}

- (IBAction)unwindToPatientData:(UIStoryboardSegue*)sender
{
    // There aren't really any extra actions that we need to do here
    // The edit view controller will have already edited the patient data;
    // This is just here for the Unwind segue;
    [self.tableView reloadData];
}

@end
