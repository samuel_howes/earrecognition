//
//  EARMatchResultsTableViewController.m
//  EarRecognition
//
//  Created by Sam Howes on 4/27/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import "EARMatchResultsTableViewController.h"
#import "EARImageMatcher.h"
#import "EARPatientsDataStore.h"
#import "EARPatient.h"
#import "EARIndividualPatientTableViewController.h"

const int kPercentMatchViewTag                  = 1;
const int kPatientNameLabelViewTag              = 2;
const int kPatientDescriptorLabelViewTag        = 3;
NSString *const kViewIndividualMatchSegue       = @"ViewIndividualMatchSegue";

@interface EARMatchResultsTableViewController ()
{
    NSInteger selectedIndex;
}

// we'll be using this a lot, so we won't bother with the shared instance sillyness all the time
@property (strong, nonatomic) EARImageMatcher *imageMatcher;

@property (strong, nonatomic) EARPatientsDataStore *patientDataStore;

@end

@implementation EARMatchResultsTableViewController
@synthesize imageMatcher;
@synthesize patientDataStore;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    imageMatcher = [EARImageMatcher sharedInstance];
    patientDataStore = [EARPatientsDataStore sharedInstance];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[imageMatcher patientIDsArrayFromLastMatch] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PatientMatchResultCell" forIndexPath:indexPath];
    
    NSNumber *patientID = [[imageMatcher patientIDsArrayFromLastMatch] objectAtIndex:indexPath.row];
    EARPatient *patient = [patientDataStore patientWithID:[patientID integerValue]];
    
    double distance = [[[imageMatcher distancesArrayFromLastMatch] objectAtIndex:indexPath.row] doubleValue];
    [(UILabel *)[cell viewWithTag:kPercentMatchViewTag] setText:[NSString stringWithFormat:@"%.2f", distance]];
    [(UILabel *)[cell viewWithTag:kPatientNameLabelViewTag] setText:[NSString stringWithFormat:@"%@ %@", patient.firstName, patient.lastName]];
    [(UILabel *)[cell viewWithTag:kPatientDescriptorLabelViewTag] setText:[NSString stringWithFormat:@"%@", patient.descriptionString]];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Best matches";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectedIndex = indexPath.row;
    [self performSegueWithIdentifier:kViewIndividualMatchSegue sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kViewIndividualMatchSegue])
    {
        NSNumber *patientID = [[imageMatcher patientIDsArrayFromLastMatch] objectAtIndex:selectedIndex];
        EARPatient *selectedPatient = [patientDataStore patientWithID:[patientID integerValue]];
        [(EARIndividualPatientTableViewController *)[segue destinationViewController] setPatient:selectedPatient];
    }
}


@end
