//
//  EARMedicalCondition.h
//  EarRecognition
//
//  Created by Sam Howes on 4/26/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum MedicalHistoryItemTypeEnum
{
    EARDisease = 0,
    EARChronic,
    EARSpiritAnimal,
    EARMood,
    EARVaccination
} MedicalHistoryItemType;

extern const int MedicalHistoryItemMaxValue;

@interface EARMedicalHistoryItem : NSObject

@property (nonatomic) MedicalHistoryItemType itemType;
@property (strong, nonatomic) NSString *itemName;
@property (strong, nonatomic) NSString *itemDate;

- (id)initWithJSONObject:(NSDictionary *)jsonObject;

- (NSDictionary *)exportToJSONObject;

+ (NSString *)itemTypeStringForEnumValue:(MedicalHistoryItemType)value;

+ (MedicalHistoryItemType)itemTypeEnumValueForString:(NSString *)value;

@end
