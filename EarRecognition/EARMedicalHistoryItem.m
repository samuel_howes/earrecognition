//
//  EARMedicalCondition.m
//  EarRecognition
//
//  Created by Sam Howes on 4/26/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import "EARPatientsDataStore.h"
#import "EARMedicalHistoryItem.h"

@implementation EARMedicalHistoryItem
@synthesize itemDate;
@synthesize itemName;
@synthesize itemType;


const int MedicalHistoryItemMaxValue = EARVaccination;

+ (NSArray *)itemTypeStrings
{
    static NSArray *values = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        values = @[@"Disease", @"Chronic", @"Spirit Animal", @"Mood", @"Vaccination"];
    });
    return values;
}

+ (NSString *)itemTypeStringForEnumValue:(MedicalHistoryItemType)value
{
    return [[EARMedicalHistoryItem itemTypeStrings] objectAtIndex:value];
}

+ (MedicalHistoryItemType)itemTypeEnumValueForString:(NSString *)value
{
    static NSDictionary *values = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        NSArray *stringValues = [EARMedicalHistoryItem itemTypeStrings];
        NSMutableDictionary *temp = [NSMutableDictionary new];
        for (int ii = 0; ii < [stringValues count]; ++ii)
        {
            [temp setValue:[NSNumber numberWithInt:ii] forKey:[stringValues[ii] lowercaseString]];
        }
        values = temp;
    });
    return (MedicalHistoryItemType)[(NSNumber *)[values objectForKey:[value lowercaseString]] integerValue];
}

- (id)initWithJSONObject:(NSDictionary *)jsonObject
{
    self = [super init];
    if (self)
    {
        self.itemType = [EARMedicalHistoryItem itemTypeEnumValueForString:jsonObject[kPatientHistoryItemTypeJSONKey]];
        self.itemName = jsonObject[kPatientHistoryItemNameJSONKey];
        self.itemDate = jsonObject[kPatientHistoryItemDateJSONKey];
    }
    return self;
}

- (NSDictionary *)exportToJSONObject
{
    NSMutableDictionary *jsonObject = [NSMutableDictionary new];
    jsonObject[kPatientHistoryItemTypeJSONKey] = [EARMedicalHistoryItem itemTypeStringForEnumValue:self.itemType];
    jsonObject[kPatientHistoryItemNameJSONKey] = self.itemName;
    jsonObject[kPatientHistoryItemDateJSONKey] = self.itemDate;
    return jsonObject;
}

@end
