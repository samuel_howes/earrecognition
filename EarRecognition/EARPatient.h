//
//  EARPatient.h
//  EarRecognition
//
//  Created by Sam Howes on 4/26/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EARPatient : NSObject

@property (strong, nonatomic) NSNumber *patientID;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *DOB;
@property (strong, nonatomic) NSMutableArray *medicalHistory;
@property (strong, nonatomic) NSString *descriptionString;
@property (strong, nonatomic) NSNumber *age;

@property (strong, nonatomic) NSArray *SIFTEarDescriptorNamesArray;

- (id)initWithJSONObject:(NSDictionary *)jsonObject;

- (NSDictionary *)exportToJSONObject;

- (void)calculateAge;

@end
