//
//  EARPatient.m
//  EarRecognition
//
//  Created by Sam Howes on 4/26/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import "EARPatient.h"
#import "EARPatientsDataStore.h"
#import "EARMedicalHistoryItem.h"

@implementation EARPatient
@synthesize DOB;
@synthesize firstName;
@synthesize lastName;
@synthesize SIFTEarDescriptorNamesArray;
@synthesize medicalHistory;
@synthesize patientID;
@synthesize descriptionString;
@synthesize age;

- (id)initWithJSONObject:(NSDictionary *)jsonObject
{
    self = [super init];
    if (self)
    {
        self.patientID = jsonObject[kPatientIDJSONKey];
        NSArray *names = [(NSString *)jsonObject[kPatientNameJSONKey] componentsSeparatedByString:@" "];
        self.firstName = [names objectAtIndex:0];
        self.lastName = [names objectAtIndex:1];
        self.DOB = jsonObject[kPatientDOBJSONKey];
        self.SIFTEarDescriptorNamesArray = [jsonObject objectForKey:kPatientSIFTEarDescriptorNamesJSONKey];
        
        self.medicalHistory = [NSMutableArray new];
        for (NSDictionary *historyItem in jsonObject[kPatientHistoryArrayJSONKey])
        {
            [self.medicalHistory addObject:[[EARMedicalHistoryItem alloc] initWithJSONObject:historyItem]];
        }
        [self calculateAge];
        
    }
    return self;
}

- (void)calculateAge
{
    // Write the description string, currently it is just how many years old the patient is
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM/dd/yyyy"];
    NSDate *dob = [df dateFromString:DOB];
    
    if (dob)
    {
        NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
        // pass as many or as little units as you like here, separated by pipes
        NSUInteger units = NSYearCalendarUnit;
        NSDateComponents *components = [gregorian components:units fromDate:dob toDate:[NSDate date] options:0];
        
        descriptionString = [NSString stringWithFormat:@"%ld yrs", (long)[components year]];
        age = [NSNumber numberWithInteger:[components year]];
    }
    else
    {
        descriptionString = @"? yrs";
        age = [NSNumber numberWithInt:0];
    }
    
}

- (NSDictionary *)exportToJSONObject
{
    NSMutableArray *patientHistoryJSONArray = [NSMutableArray new];
    
    for (EARMedicalHistoryItem *item in self.medicalHistory)
    {
        [patientHistoryJSONArray addObject:[item exportToJSONObject]];
    }
    
    NSMutableDictionary *jsonObject = [NSMutableDictionary new];
    jsonObject[kPatientIDJSONKey] = self.patientID;
    jsonObject[kPatientNameJSONKey] = [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
    jsonObject[kPatientDOBJSONKey] = self.DOB;
    jsonObject[kPatientHistoryArrayJSONKey] = patientHistoryJSONArray;
    if (SIFTEarDescriptorNamesArray)
    {
        jsonObject[kPatientSIFTEarDescriptorNamesJSONKey] = SIFTEarDescriptorNamesArray;
    }
    return jsonObject;
}

@end
