//
//  EARPatientBrowserViewController.m
//  EarRecognition
//
//  Created by Sam Howes on 4/3/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//
#import "EARPatientBrowserViewController.h"
#import "EARImageMatcherViewController.h"
#import "EARPatientsDataStore.h"
#import "EARIndividualPatientTableViewController.h"

NSString *const kAnalyzeImageSegueIdentifier = @"AnalyzeImageSegue";
NSString *const kViewPatientSegueIdentifier = @"ViewPatientSegue";

@interface EARPatientBrowserViewController () <ImageMatcherControllerDelegate>
{
    NSInteger selectedIndex;
}

@property (strong, nonatomic) EARPatientsDataStore *patientsDataStore;

- (IBAction)cameraButtonWasPressed:(id)sender;

- (IBAction)actionButtonWasPressed:(id)sender;

@end

@implementation EARPatientBrowserViewController
@synthesize patientsDataStore;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    patientsDataStore = [EARPatientsDataStore sharedInstance];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setToolbarHidden:NO];
    [self.tableView reloadData];
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/****************************************************************************/
/*                         Camera Actions                                   */
/****************************************************************************/

- (IBAction)cameraButtonWasPressed:(id)sender
{
    [self performSegueWithIdentifier:kAnalyzeImageSegueIdentifier sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    [self.navigationController setToolbarHidden:YES];
    
    if ([[segue identifier] isEqualToString:kViewPatientSegueIdentifier])
    {
        EARPatient *selectedPatient = [[patientsDataStore patientsArray] objectAtIndex:selectedIndex];
        [(EARIndividualPatientTableViewController *)[segue destinationViewController] setPatient:selectedPatient];
    }
}

- (void)imageMatcherViewControler:(EARImageMatcherViewController *)imageMatcherViewController
           didMatchImageToPatient:(EARPatient *)patient
{
    if (patient != nil)
    {
        
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionButtonWasPressed:(id)sender
{
    /*UIActionSheet *chooseImageActionSheeet =
	[[UIActionSheet alloc] initWithTitle:nil delegate:nil
					   cancelButtonTitle:@"Cancel"
				  destructiveButtonTitle:nil
					   otherButtonTitles:@"Upload Data to Server", @"Download Data from Server", nil];
	
	[chooseImageActionSheeet showInView:self.view]; */
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[patientsDataStore patientsArray] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *const kPatientCellReuseIdentifier = @"PatientCell";
    static const int kPatientNameViewTag = 1;
    static const int kPatientDescriptionViewTag = 2;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPatientCellReuseIdentifier forIndexPath:indexPath];
    
    EARPatient *patient = [[patientsDataStore patientsArray] objectAtIndex:indexPath.row];
    [(UILabel *)[cell viewWithTag:kPatientNameViewTag] setText:[NSString stringWithFormat:@"%@ %@", patient.firstName, patient.lastName]];
    [(UILabel *)[cell viewWithTag:kPatientDescriptionViewTag] setText:patient.descriptionString];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectedIndex = indexPath.row;
    [self performSegueWithIdentifier:kViewPatientSegueIdentifier sender:self];
}


@end
