//
//  EARPatientsDataStore.h
//  EarRecognition
//
//  Created by Sam Howes on 4/26/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EARPatient.h"

extern NSString *const kDatabaseFilenameJSONKey;
extern NSString *const kNumberOfPatientsJSONKey;
extern NSString *const kLastModifiedJSONKey;
extern NSString *const kPatientsArrayJSONKey;
extern NSString *const kPatientIDJSONKey;
extern NSString *const kPatientNameJSONKey;
extern NSString *const kPatientDOBJSONKey;
extern NSString *const kPatientHistoryArrayJSONKey;
extern NSString *const kPatientHistoryItemTypeJSONKey;
extern NSString *const kPatientHistoryItemNameJSONKey;
extern NSString *const kPatientHistoryItemDateJSONKey;
extern NSString *const kPatientSIFTEarDescriptorNamesJSONKey;


@interface EARPatientsDataStore : NSObject

@property (strong, nonatomic) NSString *JSONDatabasePath;

@property (strong, nonatomic) NSString *descriptorsDatabaseFilename;
@property (strong, nonatomic) NSNumber *numberOfPatients;
@property (strong, nonatomic) NSString *lastModified;
@property (strong, nonatomic) NSMutableArray *patientsArray;

+ (EARPatientsDataStore *)sharedInstance;

- (void)importPatientsFromJSONFile:(NSString *)JSONPath andOverrideWithDescriptorsArray:(NSArray *)descriptorsArray;

- (NSArray *)importPatientsFromJSONFile:(NSString *)JSONPath;

- (void)exportPatientsToJSONFile:(NSString *)JSONPath;

- (EARPatient *)patientWithID:(NSInteger)patientID;

- (NSArray *)getPatientIDsArray;

- (EARPatient *)createNewPatient;

- (void)commitNewPatient:(EARPatient *)newPatient;

@end
