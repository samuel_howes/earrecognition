//
//  EARPatientsDataStore.m
//  EarRecognition
//
//  Created by Sam Howes on 4/26/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import "EARPatientsDataStore.h"
#import "EARPatient.h"

NSString *const kDatabaseFilenameJSONKey                = @"descriptorsDatabaseFilename";
NSString *const kNumberOfPatientsJSONKey                = @"numberOfPatients";
NSString *const kLastModifiedJSONKey                    = @"lastModified";
NSString *const kPatientsArrayJSONKey                   = @"patients";
NSString *const kPatientIDJSONKey                       = @"ID";
NSString *const kPatientNameJSONKey                     = @"name";
NSString *const kPatientDOBJSONKey                      = @"DOB";
NSString *const kPatientHistoryArrayJSONKey             = @"medicalHistory";
NSString *const kPatientHistoryItemTypeJSONKey          = @"type";
NSString *const kPatientHistoryItemNameJSONKey          = @"name";
NSString *const kPatientHistoryItemDateJSONKey          = @"date";
NSString *const kPatientSIFTEarDescriptorNamesJSONKey   = @"SIFTEarDescriptorNames";


@interface EARPatientsDataStore ()

@property (nonatomic) BOOL hasBeenModified;

@end


@implementation EARPatientsDataStore

@synthesize JSONDatabasePath;
@synthesize descriptorsDatabaseFilename;
@synthesize hasBeenModified;
@synthesize patientsArray;
@synthesize numberOfPatients;
@synthesize lastModified;

+ (EARPatientsDataStore *)sharedInstance
{
    static EARPatientsDataStore *this = nil;
    
    if (this == nil)
    {
        this = [[EARPatientsDataStore alloc] init];
    }
    return this;
}

+ (NSString *)pathForDefaultBundleJSONDatabase
{
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"Patients" ofType:@"json"];
    return jsonPath;
}

- (void)importPatientsFromJSONFile:(NSString *)JSONPath andOverrideWithDescriptorsArray:(NSArray *)descriptorsArray
{
    [self importPatientsFromJSONFile:JSONPath];
    if ([patientsArray count] != [descriptorsArray count])
    {
        @throw [NSException exceptionWithName:@"Patients Inconsistency" reason:@"The specified descriptors array does not have the same number of elements as the loaded JSON Database" userInfo:nil];
    }
    for (int ii = 0; ii < [patientsArray count]; ++ii)
    {
        [(EARPatient *)patientsArray[ii] setSIFTEarDescriptorNamesArray:(NSArray *)descriptorsArray[ii]];
    }
}

- (NSArray *)importPatientsFromJSONFile:(NSString *)JSONPath
{
    if (!JSONPath)
    {
        JSONPath = [EARPatientsDataStore pathForDefaultBundleJSONDatabase];
        NSLog(@"[EARPatientDataStore] Importing patients from the default Bundle database: %@", JSONPath);
    }
    else
    {
        NSLog(@"[EARPatientDataStore] Importing patients from file: %@", JSONPath);
    }
    JSONDatabasePath = JSONPath;
    
    NSInputStream *JSONStream = [NSInputStream inputStreamWithFileAtPath:JSONPath];
    [JSONStream open];
    NSError *jsonError = nil;
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithStream:JSONStream options:0 error:&jsonError];
    [JSONStream close];
    
    if (jsonError)
    {
        @throw [NSException exceptionWithName:@"EARPatientsDataStore" reason:@"Error reading JSON file" userInfo:nil];
    }
    
    NSMutableArray *allPatientSIFTDescriptorsArray = [NSMutableArray new];
    @try {
        self.descriptorsDatabaseFilename    = jsonObject[kDatabaseFilenameJSONKey];
        self.numberOfPatients               = jsonObject[kNumberOfPatientsJSONKey];
        self.lastModified                   = jsonObject[kLastModifiedJSONKey];
        self.patientsArray                  = [NSMutableArray new];
        
        NSArray *patientDescriptorsArray;
        for (NSDictionary *jsonPatient in jsonObject[kPatientsArrayJSONKey])
        {
            [self.patientsArray addObject:[[EARPatient alloc] initWithJSONObject:jsonPatient]];
            patientDescriptorsArray = [[patientsArray lastObject] SIFTEarDescriptorNamesArray];
            if (patientDescriptorsArray != nil)
            {
                [allPatientSIFTDescriptorsArray addObject:patientDescriptorsArray];
            }
        }
        
    }
    @catch (NSException *exception) {
        @throw [NSException exceptionWithName:@"EARPatientsDataStore" reason:@"Error using data from JSON file" userInfo:nil];
    }
    
    if ([self.patientsArray count] != [self.numberOfPatients integerValue])
    {
        @throw [NSException exceptionWithName:@"EARPatientDataStore" reason:@"Number of patients in JSON file is inconsistent" userInfo:nil];
    }
    
    NSLog(@"[EARPatientsDataStore] Successfully imported data for %d patients", [self.numberOfPatients integerValue]);
    return allPatientSIFTDescriptorsArray;
}

- (void)exportPatientsToJSONFile:(NSString *)JSONPath
{
    if (!JSONPath)
    {
        JSONPath = JSONDatabasePath;
    }
    
    NSLog(@"[EARPatientDataStore] Exporting data for %d patients to file: %@", [self.numberOfPatients integerValue], JSONPath);
    
    NSURL *dataPath = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    JSONPath = [[dataPath path] stringByAppendingPathComponent:@"PatientsExported.json"];
    
    NSMutableArray *patientsJSONArray = [NSMutableArray new];
    for (EARPatient *patient in self.patientsArray)
    {
        [patientsJSONArray addObject:[patient exportToJSONObject]];
    }
    
    NSMutableDictionary *jsonObject = [NSMutableDictionary new];
    
    jsonObject[kDatabaseFilenameJSONKey]    = self.descriptorsDatabaseFilename;
    jsonObject[kNumberOfPatientsJSONKey]    = self.numberOfPatients;
    jsonObject[kLastModifiedJSONKey]        = (hasBeenModified ? @"INVALID": self.lastModified);
    jsonObject[kPatientsArrayJSONKey]       = patientsJSONArray;
    
    NSOutputStream *JSONStream = [NSOutputStream outputStreamToFileAtPath:JSONPath append:NO];
    
    NSData *jsonData;
    NSError *jsonError;
    jsonData = [NSJSONSerialization dataWithJSONObject:jsonObject options:NSJSONWritingPrettyPrinted error:&jsonError];
    
    if (jsonError)
    {
        @throw [NSException exceptionWithName:@"EARPatientsDataStore" reason:@"Error exporting to JSON data" userInfo:nil];
    }
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    [jsonString writeToFile:JSONPath atomically:NO encoding:NSUTF8StringEncoding error:&jsonError];
    
    if (jsonError)
    {
        @throw [NSException exceptionWithName:@"EARPatientsDataStore" reason:@"Error writing data to the JSON file" userInfo:nil];
    }
    
    NSLog(@"[EARPatientsDataStore] Export complete");
}

- (EARPatient *)patientWithID:(NSInteger)patientID
{
    return (EARPatient *)patientsArray[patientID - 1];
}

- (NSArray *)getPatientIDsArray
{
    NSMutableArray *patientIDsArray = [NSMutableArray new];
    for (EARPatient *patient in patientsArray)
    {
        [patientIDsArray addObject:[patient patientID]];
    }
    return patientIDsArray;
}

- (EARPatient *)createNewPatient
{
    EARPatient *newPatient = [[EARPatient alloc] init];
    NSNumber *newPatientID = [NSNumber numberWithInt:[patientsArray count] + 1];
    [newPatient setPatientID:newPatientID];
    [newPatient setDescriptionString:[NSString stringWithFormat:@"ID:%@", newPatientID]];
    return newPatient;
}

- (void)commitNewPatient:(EARPatient *)newPatient
{
    [patientsArray addObject:newPatient];
    numberOfPatients = [NSNumber numberWithInteger:[patientsArray count]];
}

@end
