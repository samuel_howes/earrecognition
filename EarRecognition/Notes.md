Developer Notes
===============

Tasks:

- Add a new patient with imageMatcher
    - Commit last descriptors
    - Generate descriptor Names
    - Add descriptor names to internal array
    - Set descriptor names of new patient
    - Add patient to the patients array
        - increment number of patients

o validate input
o add medical history items

+ Add an "Add patient" button
+ Enable editing of a patients data

+ Browse the entire patients store
    + View one patient
    + View a patient's history

+ Show results from image match
    + Display the best match for the ear
    + Display the top 5 matches for the ear?
    + segue to the individual patient from the image matcher
    ? Display the score for that match?
    o Search the patients store

+ Finalize the image matching interface
    + Decide on what gets displayed and when

+ Match Descriptor data to User data in the file
    + Parse JSON and add a list of descriptors for each person
    + Load JSON File from disk
    + Save JSON File again
    + Find out which images go with which

+ Store featurevectors to a file.
+ Load data from file

+ Adapt original opencvCode
    + Downsample the image --> Alex
    + Initialize with stored data
    + Crop the image
    + Get it working
    + Pass it a UIImage

+ Draw an overlay box for the user to center the ear

