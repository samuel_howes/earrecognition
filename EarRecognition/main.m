//
//  main.m
//  EarRecognition
//
//  Created by Sam Howes on 4/3/14.
//  Copyright (c) 2014 Sam Howes. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EARAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EARAppDelegate class]));
    }
}
