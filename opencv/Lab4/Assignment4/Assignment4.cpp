
#include "stdafx.h"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

using namespace cv;
using namespace std;

void readCSV(vector<string>& vec, string filename);

int main(int argc, char* argv[])
{	
	
	cout<<"START"<<endl;//DEBUG
	
	vector<vector<KeyPoint>> allKeypoints;
	vector<vector<DMatch>>allMatches;
	vector<Mat> allDescriptors;
	vector<Mat> allImages;
	vector<string>imageFileNames;
	vector<float>vectEuclidDistSums;
	vector<float>vectSortedEuclidDistSums;
	Mat matEuclidDistSums;
	Mat matSortedEuclidDistSums;

	SiftFeatureDetector detector;
	BFMatcher matchmaker;

	readCSV(imageFileNames,"filenames.csv");

	int numImages = imageFileNames.size();
	int testImageIndex = 0;

	cout<<imageFileNames.size() << endl;//DEBUG

	//cout << argv[1] << " " << argv[2] << " " << argv[3] << endl;//DEBUG
	//cvtColor(colorMat, greyMat, CV_BGR2GRAY);
    for (int i = 0; i < numImages;i++){
		allImages.push_back(imread(imageFileNames[i]));//CHANGE FOR CSV
		//namedWindow("test",1);
		//imshow("test",allImages[i]);
		//waitKey(0);
	}

	cout<<"VECTORS"<<endl;//DEBUG

	for(int i = 0;i < numImages;i++){
		vector<KeyPoint> temp;
		detector.detect(allImages[i],temp);
		allKeypoints.push_back(temp);
	}

	cout<<"COMPUTE"<<endl;//DEBUG

	for(int i = 0;i < numImages;i++){
		Mat temp; 
		detector.compute(allImages[i],allKeypoints[i],temp);
		allDescriptors.push_back(temp);
	}

	cout<<"MATCH"<<endl;//DEBUG
	//trouble makers
	//024_2.bmp --> index 84
	//122_2.bmp --> index 475
	//cout << "001_1.bmp keypoints: " <<allKeypoints[0].size() << " descriptors: " << allDescriptors[0].size()<<endl;//DEBUG
	//cout << "024_2.bmp keypoints: " <<allKeypoints[84].size() << " descriptors: " << allDescriptors[84].size()<<endl;//DEBUG
	//cout << "0122_2.bmp keypoints: " <<allKeypoints[475].size() << " descriptors: " << allDescriptors[475].size()<<endl;//DEBUG

	
	for(int i = 0;i < numImages;i++){
		vector<DMatch> temp;
		//cout<< "At: " << i << endl;//DEBUG
		matchmaker.match(allDescriptors[testImageIndex], allDescriptors[i], temp);
		if(!temp.empty()){
			allMatches.push_back(temp);
		} else {
			cout <<"Empty at: "<< i << endl;//DEBUG
		}
	}

	for(int i = 0;i< numImages;i++){
		cout << allKeypoints[i].size() << " " <<  allMatches[i].size() << endl;//DEBUG
	}	

	for(int i = 0;i< numImages;i++){
		float tempSum = 0;
		for(int j = 0;j < allMatches[i].size();j++){
			tempSum += allMatches[i][j].distance;
		}
		vectEuclidDistSums.push_back(tempSum);
	}
	matEuclidDistSums = Mat(vectEuclidDistSums);

	cout << matEuclidDistSums << endl;//DEBUG
	
	sortIdx(matEuclidDistSums,matSortedEuclidDistSums,CV_SORT_EVERY_COLUMN + CV_SORT_ASCENDING);
	
	cout << matSortedEuclidDistSums << endl;//DEBUG
	

    while(1 == 1){

        char key = waitKey(33);
        if(key == 'q')
        {
            break;
        }
    }	
}

void readCSV(vector<string>& vec, string filename){
	
	ifstream infile(filename.c_str());
	int index = 0;

	if(infile){
		string line;
		while(getline(infile,line)){
			istringstream sep(line);
			string result;
			while(getline(sep, result,',')){
				vec.push_back(result);
				index++;
			}
		}
	}
}

